package Jahr1.hotelzimmer;

public class hotelzimmer {

    private int zimmernummer;
    private double preis;
    private boolean belegt;

    public void zimmerEinrichten(int zimmernummer, double preis, boolean belegt) {
        this.zimmernummer = zimmernummer;
        this.preis = preis;
        this.belegt = belegt;
    }

    public String zimmerDrucken(){
        return "Zimmernummer: " + zimmernummer + "\n" +
                "Preis: " + preis +  "\n" +
                "Belegt: " + belegt + "\n";
    }
}
