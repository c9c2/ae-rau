package Jahr1.kraftstoff;

public class PKW {

    private int oldTank;
    private int newTank;
    private double currentLiter;

    PKW() {
        this(10200, 10800, 42.5);
    }

    PKW(int oldTank, int newTank, double currentLiter) {
        this.oldTank = oldTank;
        this.newTank = newTank;
        this.currentLiter = currentLiter;
    }

    public double verbrauch() {
        return (this.currentLiter * 100) / (this.newTank - this.oldTank);
    }
}
