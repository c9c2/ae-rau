package Jahr1.kraftstoff;

import java.util.Scanner;

public class starter {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        PKW pkwA = new PKW();

        try {
            System.out.println("Please enter the old tank value:");
            int oldTank = sc.nextInt();
            System.out.println("Old tank: " + oldTank);
            System.out.println("Please enter the new tank value: \n!!! old tank must be less than new tank !!!");
            int newTank = sc.nextInt();
            System.out.println("New tank: " + newTank);
            System.out.println("Please enter the current liter value: \n!!! the double value must be [,] separated");
            double currentLiter = sc.nextDouble();
            System.out.println("Current liter: " + currentLiter);

            PKW pkwB = new PKW(oldTank, newTank, currentLiter);

            System.out.println(pkwA.verbrauch());

            System.out.println(pkwB.verbrauch());
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
