package Jahr1.rechnungKorrektur;

public class uebungsAufgabe {

    public static void main(String[] args) {
        int intx, inty, intz;
        byte byteWert, byteErgebnis;
        short shortZahl, shortErgebnis;
        double kommazahl, kommaErgebnis;

        intx = 21;
        byteWert = 34;
        shortZahl = -965;
        kommazahl = 45000.22;
        intz = -32772;
        inty = shortZahl;
        kommaErgebnis = (shortZahl * 3.5) + (byteWert/intx);          //1
        System.out.println("Kommaergebnis: " + kommaErgebnis);
        kommaErgebnis = shortZahl * ((3.5 + byteWert)/intx);          //2
        System.out.println("Kommaergebnis: " + kommaErgebnis);
        byteErgebnis = (byte) intx;
        System.out.println("byteErgebnis: " + byteErgebnis);
        shortErgebnis = (short) intz;
        System.out.println("shortErgebnis: " + shortErgebnis);
        shortErgebnis = (short) (shortErgebnis + 2);                                             //3
        System.out.println("shortErgebnis plus 2: " + shortErgebnis);
    }
}
