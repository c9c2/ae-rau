package Jahr1.zinsrechnung;

import java.util.Scanner;

public class zinsrechnen {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int jahr = sc.nextInt();
        int betrag = sc.nextInt();
        zinsBerechnung(jahr, betrag);
    }

    private static void zinsBerechnung(int jahr, double betrag) {
        if ((jahr > 20 || jahr < 0) || betrag > 100000 || betrag < 0) {
            System.out.println("ERR: year = " +  jahr + " value: " + betrag);
        } else {
            for (int i = 1; i <= jahr; i++) {
                betrag *= 1.15;
                System.out.println("End Betrag: " + betrag + " für das Jahr: " + i);
            }
        }
    }
}
