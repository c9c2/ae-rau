package Jahr1.testresult;

public class TestResult {

    public static void main(String[] args) {
        System.out.println(testBewerten(4));
        System.out.println(testBewerten(7));
        System.out.println(testBewerten(10));
        System.out.println(testBewerten(11));
        System.out.println(testBewerten(-3));
    }


    private static String testBewerten(int punktzahl) {
        if (punktzahl <= 10 && punktzahl >= 7) {
            return "Der Test ist bestanden!";
        } else {
            if (punktzahl > 10 || punktzahl < 0) {
                return "FEHLER! Ungültige Punktzahl!";
            }
        }
        return "Der Test ist leider nicht bestanden";
    }
}
