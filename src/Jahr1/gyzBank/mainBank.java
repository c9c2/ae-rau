package Jahr1.gyzBank;

public class mainBank {
    public static void main(String[] args) {
        konten kontoEins = new konten(8428347, 3000);
        konten kontoZwei = new konten(483748, 300);

        System.out.println("Konto 1 current balance: " + kontoEins.getBestand());
        System.out.println("Konto 2 current balance: " + kontoZwei.getBestand());

        System.out.println("Konto 1 transfers 200 to Konto 2");

        kontoEins.abgang(200);
        kontoZwei.zugang(200);

        System.out.println("Konto 1 current balance: " + kontoEins.getBestand());
        System.out.println("Konto 2 current balance: " + kontoZwei.getBestand());

    }
}
