package Jahr1.gyzBank;

public class konten {

    private int kontoNummer;
    private int bestand;

    konten() {
        this(0,0);
    }

    konten(int kontoNummer, int bestand) {
        this.kontoNummer = kontoNummer;
        this.bestand = bestand;
    }

    public void abgang(int minus) {
        //System.out.println("Current balance: " + this.bestand);
        this.bestand = this.bestand - minus;
        //System.out.println("New current balance: " + this.bestand);
    }

    public void zugang(int plus) {
        //System.out.println("Current balance: " + this.bestand);
        this.bestand = this.bestand + plus;
        //System.out.println("New current balance: " + this.bestand);
    }

    public int getKontoNummer() {
        return kontoNummer;
    }

    public int getBestand() {
        return bestand;
    }
}
