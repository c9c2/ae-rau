package Jahr1.autovermietung;

import java.util.Scanner;

public class preisermittlung {

    private int autoNummer;
    private int mietTage;
    private double tagePreis;
    private double kmPreis;
    private double gefahreneKm;
    private double gesamtPreis;

    preisermittlung() {
        this.autoNummer = 0;
        this.mietTage = 0;
        this.tagePreis = 0;
        this.kmPreis = 0;
        this.gefahreneKm = 0;
    }

    public void datenEinlesen() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Bitte gib die Auto Nummer ein: ");
        this.autoNummer = sc.nextInt();
        System.out.println("Bitte gib die Miet Tage ein: ");
        this.mietTage = sc.nextInt();
        System.out.println("Bitte gib den Tages Preis ein: ");
        this.tagePreis = sc.nextInt();
        System.out.println("Bitte gib den KM Preis ein: ");
        this.kmPreis = sc.nextDouble();
        System.out.println("Bitte gib die gefahrenen KM ein: ");
        this.gefahreneKm = sc.nextDouble();
    }

    public void gesamtPreisBerechnen() {
        this.gesamtPreis = (this.mietTage * this.tagePreis) + (this.gefahreneKm * this.kmPreis);
    }

    public double gesamtPreisAusgeben() {
        return this.gesamtPreis;
    }
}
