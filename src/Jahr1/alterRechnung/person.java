package Jahr1.alterRechnung;

import java.util.GregorianCalendar;

public class person {
    private int tag;
    private int monat;
    private int jahr;
    private String name;

    person(int tag, int monat, int jahr, String name) {
        this.tag = tag;
        this.monat = monat;
        this.jahr = jahr;
        this.name = name;
    }

    public int getAlter() {
        GregorianCalendar gc = new GregorianCalendar();
        GregorianCalendar od = new GregorianCalendar(this.jahr, this.monat, this.tag);

        int currentYear = gc.get(GregorianCalendar.YEAR);
        int currentMonth = gc.get(GregorianCalendar.MONTH) + 1;
        int currentDay = gc.get(GregorianCalendar.DAY_OF_MONTH);

        int personYear = od.get(GregorianCalendar.YEAR);
        int personMonth = od.get(GregorianCalendar.MONTH);
        int personDay = od.get(GregorianCalendar.DAY_OF_MONTH);

        /*
        System.out.println(currentYear);
        System.out.println(currentMonth);
        System.out.println(currentDay);

        System.out.println(personYear);
        System.out.println(personMonth);
        System.out.println(personDay);
        */


        return 0;
    }

    public void druckeInfo() {
        System.out.println("Name: " + this.name);
        System.out.println("Jahr: " + this.jahr);
        System.out.println("Monat: " + this.monat);
        System.out.println("Tag: " + this.tag);
    }

    public int getTageBisGeburtstag() {
        return 0;
    }

    public void setGeburtstag(int t, int m, int y) {
        this.tag = t;
        this.monat = m;
        this.jahr = y;
    }

    public boolean istVolljaehrig() {
        return true;
    }

    public void setName(String name) {
        this.name = name;
    }
}
