package Jahr1.automobilverkehr;

public class auto {

    private String farbe;
    private String modell;
    private String antriebsart;
    private int geschwindigkeit;
    private int anzahlTueren;

    private int currentSpeed = 0;

    auto() {
        this(null, null, null, 0, 0);
    }

    auto(String farbe, String modell, String antriebsart, int geschwindigkeit, int anzahlTueren) {
        this.farbe = farbe;
        this.modell = modell;
        this.antriebsart = antriebsart;
        this.geschwindigkeit = geschwindigkeit;
        this.anzahlTueren = anzahlTueren;
    }

    public void beschleunigen() {
        //System.out.println("Beschleunigen");
        this.currentSpeed++;
    }

    public void bremsen() {
        //System.out.println("Bremsen");
        this.currentSpeed--;
    }

    public void tuerOeffnen() {
        System.out.println("Tür öffnen");
    }

    public void tuerSchliessen() {
        System.out.println("Tür schließen");
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public String getFarbe() {
        return farbe;
    }

    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }

    public String getModell() {
        return modell;
    }

    public void setModell(String modell) {
        this.modell = modell;
    }

    public String getAntriebsart() {
        return antriebsart;
    }

    public void setAntriebsart(String antriebsart) {
        this.antriebsart = antriebsart;
    }

    public int getGeschwindigkeit() {
        return geschwindigkeit;
    }

    public void setGeschwindigkeit(int geschwindigkeit) {
        this.geschwindigkeit = geschwindigkeit;
    }

    public int getAnzahlTueren() {
        return anzahlTueren;
    }

    public void setAnzahlTueren(int anzahlTueren) {
        this.anzahlTueren = anzahlTueren;
    }

}
