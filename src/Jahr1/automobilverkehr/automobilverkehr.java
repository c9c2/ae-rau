package Jahr1.automobilverkehr;

public class automobilverkehr {

    public static void main(String[] args) {

        auto autoA = new auto();
        auto autoB = new auto("rot","cl-500","Automatik",2,5);

        autoA.beschleunigen();
        autoB.tuerOeffnen();

        try {
            for (int i = 0; i < 120; i++) {
                autoB.beschleunigen();
                System.out.println("autoB current speed: " + autoB.getCurrentSpeed() + "km/h");
                Thread.sleep(100);
            }

            System.out.println("---------------------------------------------------------------------");

            for (int j = 0; j < 60; j++) {
                autoB.bremsen();
                System.out.println("autoB current speed: " + autoB.getCurrentSpeed() + "km/h");
                Thread.sleep(100);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
